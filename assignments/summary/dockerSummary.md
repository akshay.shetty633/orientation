Why docker ?

It becomes hard to manage dependencies as we build large projects and if we want to deploy them in various environments. So we will be using docker to deploy our product. For this you don't need to master docker but need to know the basics.


Docker Terminologies

1. Docker

Docker is a program for developers to develop, and run applications with containers.


2. Docker Image

A Docker image is contains everything needed to run an applications as a container. This includes:

code
runtime
libraries
environment variables
configuration files


The image can then be deployed to any Docker environment and as a container.


3. Container

A Docker container is a running Docker image.
From one image you can create multiple containers .


4. Docker Hub

Docker Hub is like GitHub but for docker images and containers.

Docker Engine
Docker Engine is a client-server application with these major components:  
1. A server which is a type of long-running program called a daemon process (the dockerd command). 
2. A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do. 
3. A command line interface (CLI) client (the docker command). 

![ALT](https://gitlab.com/vaibhaviparanjpe01/orientation/-/raw/master/extras/engine.png)


The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI. The daemon creates and manages Docker objects, such as images, containers, networks, and volumes.
What can I use Docker for?
Fast, consistent delivery of your applications
Docker streamlines the development lifecycle by allowing developers to work in standardized environments using local containers which provide your applications and services. Containers are great for continuous integration and continuous delivery (CI/CD) workflows.
Consider the following example scenario:
Your developers write code locally and share their work with their colleagues using Docker containers.
They use Docker to push their applications into a test environment and execute automated and manual tests.
When developers find bugs, they can fix them in the development environment and redeploy them to the test environment for testing and validation.
When testing is complete, getting the fix to the customer is as simple as pushing the updated image to the production environment.
Responsive deployment and scaling
Docker’s container-based platform allows for highly portable workloads. Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.
Docker’s portability and lightweight nature also make it easy to dynamically manage workloads, scaling up or tearing down applications and services as business needs dictate, in near real time.

Docker architecture

The architecture of Docker uses a client-server model and consists of the Docker’s Client, Docker Host, Network and Storage components, and the Docker Registry/Hub. Let’s look at each of these in some detail.

Docker’s Client
Docker users can interact with Docker through a client. When any docker commands runs, the client sends them to dockerd daemon, which carries them out. Docker API is used by Docker commands. It is possible for Docker client to communicate with more than one daemon.

Docker Host
The Docker host provides a complete environment to execute and run applications. It comprises of the Docker daemon, Images, Containers, Networks, and Storage. As previously mentioned, the daemon is responsible for all container-related actions and receives commands via the CLI or the REST API. It can also communicate with other daemons to manage its services.

![ALT](https://docs.docker.com/engine/images/architecture.svg)

Docker objects
When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects. This section is a brief overview of some of those objects.

IMAGES
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.
You might create your own images or you might only use those created by others and published in a registry. To build your own image, you create a Dockerfile with a simple syntax for defining the steps needed to create the image and run it. Each instruction in a Dockerfile creates a layer in the image. When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt. This is part of what makes images so lightweight, small, and fast, when compared to other virtualization technologies.

CONTAINERS
A Docker container is a running Docker image.
From one image you can create multiple containers


virtual machine vs containers:

![ALT](https://images.idgesg.net/images/article/2017/06/virtualmachines-vs-containers-100727624-large.jpg)



Docker Installation:

 Ubuntu 16.04.

Uninstall the older version of docker if is already installed

$ sudo apt-get remove docker docker-engine docker.io containerd runc

Installing CE (Community Docker Engine)

$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world

Docker basic commands:


docker ps

The docker ps command allows us to view all the containers that are running on the Docker Host.

for Example:
$ docker ps

CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute                 elated_franklin

docker start

This command starts any stopped container(s).
Example

$ docker start 30986
In the above example, Docker starts the container beginning with the container ID 30986.
$ docker start sneha
Whereas in this example, Docker starts the container named sneha.

docker stop

This command stops any running container(s).
Example

$ docker stop 30986
In the above example, Docker will stop the container beginning with the container ID 30986.
$ docker stop sneha
Whereas in this example, Docker will stop the container named sneha.

docker run

This command creates containers from docker images.
Example

$ docker run sneha

docker rm

This command deletes the containers.
Example

$ docker rm sneha

Common Operations on Dockers:

Typically, you will be using docker in the given flow.

Download/pull the docker images that you want to work with.
Copy your code inside the docker
Access docker terminal
Install and additional required dependencies
Compile and Run the Code inside docker
Document steps to run your program in README.md file
Commit the changes done to the docker.
Push docker image to the docker-hub and share repository with people who want to try your code.
