Git is the way we manage code and there is no software development without git.

What is Git?

Git is version-control software that makes collaboration with teammates super simple.
Git lets you easily keep track of every revision you and your team make during the development of your software.
Basically, if you’re not using git, you’re coding with one hand tied behind your back.


![ALT](https://gitlab.com/vaibhaviparanjpe01/orientation/-/raw/master/extras/vcs.png)


How to Install Git
Git (probably) didn’t come installed on your computer, so we have to get it there. Luckily, installing git is super easy, whether you’re on Linux, Mac, or Windows.
For Linux, open the terminal and type
sudo apt-get install git 

it Workflow
The basic Git work flow goes something like this:

You clone the repo

$ git clone <link-to-repository> 

Create a new branch

$ git checkout master
$ git checkout -b <your-branch-name>

You modify files in your working tree.
You selectively stage just those changes you want to be part of your next commit,
which adds only those changes to the staging area.

$ git add .         # To add untracked files ( . adds all files) 

You do a commit, which takes the files as they are in the staging area and stores that
snapshot permanently to your Local Git Repository.

$ git commit -sv   # Description about the commit

You do a push, which takes the files as they are in the Local Git Repository and stores
that snapshot permanently to your Remote Git Repository.

$ git push origin <branch-name>      # push changes into repository



Git Internals

Git has three main states that your files can reside in: modified, staged, and committed :

Modified means that you have changed the file but have not committed it to your
repo yet.
Staged means that you have marked a modified file in its current version to go into
your next picture/snapshot.
Committed means that the data is safely stored in your local repo in form of
pictures/snapshots.

At one time there are 3/4 different trees of your software code/repository are present.

Workspace : All the changes you make via Editor(s) (gedit, notepad, vim, nano) is
done in this tree of repository.

Staging : All the staged files go into this tree of your repository.
Local Repository : All the committed files go to this tree of your repository.
Remote Repository : This is the copy of your Local Repository but is stored in some
server on the Internet. All the changes you commit into the Local Repository are
not directly reflected into this tree. You need to push your changes to the Remote
Repository.


![ALT](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

Git Vocubalary:

Repository:
It is the collection of  full files and folder.It is called as the repo.It is king of the big space which the code is thrown into.

GitLab:
The 2nd most popular remote storage solution for git repos.

Commit:
It is last final submit. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment.

Push:
Pushing is essentially syncing your commits to GitLab.

Branch:
You can think of your git repo as a tree. The trunk of the tree, the main software, is called the Master Branch. The branches of that tree are, well, called branches. These are separate instances of the code that is different from the main codebase.

Merge:
It is like free of any bugs.Ready to become part of the primary codebase, it will get merged into the master branch. Merging is just what it sounds like: integrating two branches together.

Clone:
CLone is an exact copy of the online repository.

Fork:
Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name





